function y=convert2pixel(eyeTrace,pixelAng,threshold)

switch nargin
    case 1
        pixelAng=1;
        threshold=0;

    case 2 
        threshold=0;
        
    case 3
        
    otherwise
        error('too many input arguments');
           
end

for i=1:size(eyeTrace)
    clear x;
    x=eyeTrace(i,:);
    y=nan(1,length(x));
    y(1)=round(x(1)/pixelAng);
    for t=2:5:length(x)-5
        if abs(x(t)-y(t-1))>threshold*pixelAng
            y(t:t+4)=round(x(t)/pixelAng);
        else
            y(t:t+4)=y(t-1);
        end
    end
    y(t+5:end)=round(x(t)/pixelAng);
end
% figure; hold on; plot(x); plot(y);
% xlim([2400 3200]);
end
