clear; close all;
addpath(genpath('C:\Users\zzhao\Box\toolbox\'));
figurePath=createFolder('..\Figures\');
Fs=1E3;
%% define filters

clear bv av;
% The IIR filter we are using for online eye trace smooth.
bv{1}=[ 0.0005212926,...
    0.0020851702,...
    0.0031277554,...
    0.0020851702,...
    0.0005212926];

av{1}=[-1.8668754558,...
   0.8752161367];

% 5th order butterworth filter, cutoff=25 Hz
i=2; [bv{i},av{i}] = butter(5,25/(Fs/2)); av{i}(1)=[];

% 2nd order butterworth filter, cutoff=25 Hz, adjusted to reduce delay, https://www.dsprelated.com/showarticle/1280.php
i=3; [b,a] = butter(2,25/(Fs/2)); a(1)=[];
bv{i}=[b(1)+b(2)+b(1)*a(2), b(3)+b(1)*(a(2)-a(1)), -b(1)*a(2)]; 
bv{i}=bv{i}/sum(bv{i})*sum(b);
av{i}=a;

% 5th order butterworth filter, cutoff=50 Hz
i=4; [bv{i},av{i}] = butter(5,50/(Fs/2)); av{i}(1)=[];

% 2nd order butterworth filter, cutoff=50 Hz, adjusted to reduce delay, https://www.dsprelated.com/showarticle/1280.php
i=5; [b,a] = butter(2,50/(Fs/2)); a(1)=[];
bv{i}=[b(1)+b(2)+b(1)*a(2), b(3)+b(1)*(a(2)-a(1)), -b(1)*a(2)]; 
bv{i}=bv{i}/sum(bv{i})*sum(b);
av{i}=a;

% moving weighted average
i=6; bv{i}=normcdf(linspace(2,-0.5,10),0,1);  bv{i}=bv{i}/sum(bv{i}); av{i}=0;
% i=7; bv{i}=normcdf(linspace(2,-0.5,20),0,1);  bv{i}=bv{i}/sum(bv{i}); av{i}=0;


%% charaterize filters in frequency response and filtered example eye trace
load('..\Data\exampleEM.mat');
pixAng=0.8;%[0.4 0.6 0.8 1 1.2];
thre=0.2; ti=1;
fileNames={'original_IIR','IIR_5_25','IIR_2_25','IIR_5_50','IIR_2_50','FIR_10','FIR_20' };
clear y y_d
for pi=1:length(pixAng)
    for i=1:length(bv)
        filterChar(bv{i},av{i},x,[2900 3400],pixAng(pi),thre(ti));
        saveFigure(sprintf('%s_p%d_t%d',fileNames{i}, round(pixAng(pi)*10), round(thre(ti)*10)),figurePath);
    end
end

%% quantify performance of each filter
load('..\Data\exampleEM.mat');
vel=sgfilt(x,3,41,1);
driftBool=abs(vel)<0.2;
startTim=find(diff(driftBool)>0);
endTim=find(diff(driftBool)<0);
endTim=endTim(find(endTim>startTim(1),1):end);
L=min([length(startTim) length(endTim)]);
driftTim=[startTim(1:L)' endTim(1:L)'];
driftTim=driftTim(diff(driftTim')>100,:);

figure; hold on;
for di=1:size(driftTim,1)
    dur=driftTim(di,1):driftTim(di,2);
    plot(dur, x(dur));
end


wins=[11 61];
x_true{1}=sgfilt(x,3,11,0);
x_true{2}=sgfilt(x,3,61,0);

filtMargin=200;
pixAng=[0.4 0.6 0.8 1 1.2];
thre=0.2; ti=1;
fileNames={'original_IIR','IIR_5_25','IIR_2_25','IIR_5_50','IIR_2_50','FIR_10','FIR_20' };
clear y y_d y_d RMSE

for xi=length(x_true):-1:1
    for i=length(bv):-1:0
        
        if i>0
            fprintf('processing filter=%s\n',fileNames{i});
            y=IIR_EM_filter(x,bv{i},av{i});
        else
            disp('processing no filter');
            y=x;
        end
        tmp=x_true{xi}-y; tmp=tmp(filtMargin:end); 
        RMSE{xi}.arcmin(i+1)=sqrt(mean(tmp.^2));
        RMSE{xi}.arcmin(i+1)=sqrt(mean(tmp.^2));
        
        errV=[]; 
        for di=1:size(driftTim,1)
            dur=driftTim(di,1):driftTim(di,2);
            tmp=x_true{xi}(dur)-y(dur); tmp=tmp-mean(tmp);
            errV=[errV tmp];
        end
        RMSE{xi}.drift.arcmin(i+1)=sqrt(mean(errV.^2));

        for pi=length(pixAng):-1:1
            y_d=pixAng(pi)*convert2pixel(y,pixAng(pi),thre(ti));

            tmp=x_true{xi}-y_d; tmp=tmp(filtMargin:end); 
            RMSE{xi}.pixel(i+1,pi)=sqrt(mean(tmp.^2));

            errV=[];
            for di=1:size(driftTim,1)
                dur=driftTim(di,1):driftTim(di,2);
                tmp=x_true{xi}(dur)-y_d(dur); tmp=tmp-mean(tmp);
                errV=[errV tmp];  
            end
            RMSE{xi}.drift.pixel(i+1,pi)=sqrt(mean(errV.^2));

        end
    end
end


delay=7;
clear y y_d y_d RMSE_7ms
for xi=length(x_true):-1:1
    for i=length(bv):-1:0
        
        if i>0
            fprintf('processing filter=%s\n',fileNames{i});
            y=IIR_EM_filter(x,bv{i},av{i});
        else
            disp('processing no filter');
            y=x;
        end
        tmp=x_true{xi}(1+delay:end)-y(1:end-delay); tmp=tmp(filtMargin:end); 
        RMSE_7ms{xi}.arcmin(i+1)=sqrt(mean(tmp.^2));
        RMSE_7ms{xi}.arcmin(i+1)=sqrt(mean(tmp.^2));
        
        errV=[]; 
        for di=1:size(driftTim,1)
            dur=driftTim(di,1):driftTim(di,2)-delay;
            tmp=x_true{xi}(dur+delay)-y(dur); tmp=tmp-mean(tmp);
            errV=[errV tmp];
        end
        RMSE_7ms{xi}.drift.arcmin(i+1)=sqrt(mean(errV.^2));

        for pi=length(pixAng):-1:1
            y_d=pixAng(pi)*convert2pixel(y,pixAng(pi),thre(ti));

            tmp=x_true{xi}(1+delay:end)-y_d(1:end-delay); tmp=tmp(filtMargin:end); 
            RMSE_7ms{xi}.pixel(i+1,pi)=sqrt(mean(tmp.^2));

            errV=[];
            for di=1:size(driftTim,1)
                dur=driftTim(di,1):driftTim(di,2)-delay;
                tmp=x_true{xi}(dur+delay)-y_d(dur); tmp=tmp-mean(tmp);
                errV=[errV tmp];  
            end
            RMSE_7ms{xi}.drift.pixel(i+1,pi)=sqrt(mean(errV.^2));

        end
    end
end
save('..\data\filters_result.mat','x','wins','x_true','RMSE','RMSE_7ms');
%% plot
filtNames={'no filter','original IIR','IIR 5 25','IIR 2 25','IIR 5 50','IIR 2 50','FIR 10' };
for xi=length(x_true):-1:1
    figure('position',[100 100 800 500]); 
    subplot(1,2,1); hold on; 
    plot([0 pixAng],[RMSE{xi}.arcmin' RMSE{xi}.pixel],'lineWidth',1.5, 'marker','o');
    set(gca,'xTick',[0 pixAng],'FontSize',14);
    xlabel('pixel angle (arcmin)'); ylabel('rmse (arcmin)');
    title('whole trial');
    
    subplot(1,2,2); hold on; 
    plot([0 pixAng],[RMSE{xi}.drift.arcmin' RMSE{xi}.drift.pixel],'lineWidth',1.5, 'marker','o');
    set(gca,'xTick',[0 pixAng],'FontSize',14);
    xlabel('pixel angle (arcmin)'); ylabel('rmse (arcmin)');
    title('drift only');
    legend(filtNames);
    saveFigure(sprintf('rmse_win%d',wins(xi)),figurePath);
end


filtNames={'no filter','original IIR','IIR 5 25','IIR 2 25','IIR 5 50','IIR 2 50','FIR 10' };
for xi=length(x_true):-1:1
    figure('position',[100 100 800 500]); 
    subplot(1,2,1); hold on; 
    plot([0 pixAng],[RMSE_7ms{xi}.arcmin' RMSE_7ms{xi}.pixel],'lineWidth',1.5, 'marker','o');
    set(gca,'xTick',[0 pixAng],'FontSize',14);
    xlabel('pixel angle (arcmin)'); ylabel('rmse (arcmin)');
    title('whole trial');
    
    subplot(1,2,2); hold on; 
    plot([0 pixAng],[RMSE_7ms{xi}.drift.arcmin' RMSE_7ms{xi}.drift.pixel],'lineWidth',1.5, 'marker','o');
    set(gca,'xTick',[0 pixAng],'FontSize',14);
    xlabel('pixel angle (arcmin)'); ylabel('rmse (arcmin)');
    title('drift only');
    %legend(filtNames);
    saveFigure(sprintf('rmse_win%d_7ms',wins(xi)),figurePath);
end
