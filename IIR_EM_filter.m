function [out]=IIR_EM_filter(eyeTrace,b,a,PLOT)

    switch nargin
        case 1
          
            b=[0.0005212926,...
                0.0020851702,...
                0.0031277554,...
                0.0020851702,...
                0.0005212926];


            a=[-1.8668754558,...
                0.8752161367];
        case 2
            error('must input both a and b for the IIR filter');
            
        case 3
            
        case 4
            if a==-1 
                a=[-1.8668754558,...
                0.8752161367];
            end
            
            if b==-1
                b=[0.0005212926,...
                0.0020851702,...
                0.0031277554,...
                0.0020851702,...
                0.0005212926];
            end
        otherwise
            error('too many input arguments');
    end
    
    out(:,1:length(b))=eyeTrace(:,1:length(b));
    
    for i=size(eyeTrace,1):-1:1
        for t=1+length(b):size(eyeTrace,2)
            out(i,t)=b*eyeTrace(i,t:-1:t-length(b)+1)'-a*out(i,t-1:-1:t-length(a))';
        end
    end
    
    if exist('PLOT','var') && PLOT==1
        figure; hold on; 
        plot(eyeTrace','k'); 
        plot(out');
    end
end