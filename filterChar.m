function [y,y_d]=filterChar(b,a,exampleTrace,xRange,pixAng, thre,Fs)

    switch nargin
        case 4
            pixelAng=1; thre=0; Fs=1000;
        case 5
            thre=0; Fs=1000;
        case 6
            Fs=1000;
        case 7
        otherwise
            error('Not the number of input arguments expected');
    end

    if a(1)~=1
        a1=[1 a];
    else
        a1=a;
        a(1)=[];
    end
    [h,w] = freqz(b,a1,1000);
    
    figure('position',[10 10 1000 300]);
    subplot(1,3,1); hold on;
    w0=w/(2*pi)*Fs; 
    h0=20*log10(abs(h));
    plot(w0,h0); 
    [~,cutoff]=min(abs(h0+3));  gain_3=h0(cutoff);
    line([0 w0(cutoff)],[gain_3 gain_3],'lineStyle','--','color',0.5*ones(1,3));
    line([w0(cutoff) w0(cutoff)],[-6 gain_3],'lineStyle','--','color',0.5*ones(1,3));
    xlabel('temporal frequency (Hz)'); ylabel('gain (dB)');
    ylim([-6 0.5]);
    
    
    y=IIR_EM_filter(exampleTrace,b,a);
    y_d=convert2pixel(y,pixAng,thre);
    y_d=y_d*pixAng;
    
    subplot(1,3,2); hold on;
    plot(exampleTrace','k'); plot(y','lineWidth',1.5); plot(y_d','lineWidth',1.5);
    legend({'raw','filter','pixel'},'location','southEast');
    xlim(xRange);     xlabel('time (ms)'); ylabel('position (arcmin)'); title('no delay');
    
    delay=7; % ms
    subplot(1,3,3); hold on;
    plot(exampleTrace(1+delay:end)','k'); plot(y(1:end-delay)','lineWidth',1.5); plot(y_d(1:end-delay)','lineWidth',1.5);
    legend({'raw','filter','pixel'},'location','southEast');
    xlim(xRange);     xlabel('time (ms)'); ylabel('position (arcmin)'); title('7 ms delay');
end